import itertools
import random

class LoadBalancer:
    def __init__(self, servers):
        self.servers_by_region = {}
        self.servers = itertools.cycle(servers)
        self.server_stats = {server: 0 for server in servers}
        for server, region in servers.items():
            if region not in self.servers_by_region:
                self.servers_by_region[region] = []
            self.servers_by_region[region].append(server)

    def get_server(self, region=None):
        if region is None:
            server = next(self.servers)
        else:
            if region not in self.servers_by_region:
                raise ValueError("No servers in region {0}".format(region))
            server = random.choice(self.servers_by_region[region])
        self.server_stats[server] += 1
        return server

    def get_stats(self):
        return self.server_stats

servers = {"server1": "us-east", "server2": "us-east", "server3": "us-west", "server4": "eu-west"}
lb = LoadBalancer(servers)

for i in range(10):
    server = lb.get_server()
    print("Request {0} served by {1}".format(i+1, server))

stats = lb.get_stats()
print("Server statistics:", stats)