# Load Balancer

Distributes network traffic across multiple servers to optimize resource utilization, improve efficiency, and increase reliability.

This implementation of a load balancer in Python takes into account the region, load, and response time of each server to select the best server for a new request.

## Usage

### Creating a LoadBalancer object

To use the load balancer, first create a `LoadBalancer` object by passing a dictionary of servers and their properties to the constructor:
    
    python
    
    from load_balancer import LoadBalancer servers = { "server1": {"region": "us-east-1", "load": 2, "response_time": 100}, "server2": {"region": "us-west-1", "load": 1, "response_time": 200}, "server3": {"region": "us-west-1", "load": 0, "response_time": 150}, } lb = LoadBalancer(servers) 

The `servers` dictionary maps each server name to a dictionary of properties, including the region, current load, and response time of the server.

### Selecting a server

To select a server for a new request, call the `get_server` method of the load balancer:
    
    python
    
    server = lb.get_server(region="us-west-1") 

The `get_server` method takes an optional `region` argument, which specifies the region to select a server from. If no region is specified, the load balancer selects a server from all regions.

The `get_server` method returns the name of the selected server.

### Updating server statistics

After a request has been processed, call the `update_server_stats` method of the load balancer to update the load and response time statistics of the server that handled the request:
    
    python
    
    lb.update_server_stats(server, response_time) 

The `server` argument is the name of the server that handled the request, and the `response_time` argument is the time it took for the server to handle the request.

### Getting load balancer statistics

To get statistics about the load balancer and its servers, call the `get_stats` method of the load balancer:
    
    python
    
    stats = lb.get_stats() 

The `get_stats` method returns a dictionary containing the following keys:

* `total_requests`: the total number of requests handled by all servers
* `servers`: a dictionary mapping each server name to a dictionary of properties, including the current load, response time, number of requests, and last time used
* `regions`: a dictionary mapping each region to a dictionary of properties, including the total load, number of requests, response time, and last time used for all servers in the region

## License

This load balancer implementation is released under the MIT License.