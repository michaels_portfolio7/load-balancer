import itertools
import random
import time


class LoadBalancer:
    def __init__(self, servers):
        self.servers_by_region = {}
        self.servers = []
        self.server_stats = {}
        for server, server_data in servers.items():
            region = server_data.get("region", "default")
            self.servers.append(server)
            self.server_stats[server] = {
                "region": region,
                "load": server_data.get("load", 0),
                "response_time": server_data.get("response_time", 0),
                "requests": 0,
                "last_used": time.time(),
            }
            if region not in self.servers_by_region:
                self.servers_by_region[region] = []
            self.servers_by_region[region].append(server)

       def get_server(self, region=None):
        if region is not None and region not in self.servers_by_region:
            raise ValueError("No servers in region {0}".format(region))

        filtered_servers = [s for s in self.servers if region is None or self.server_stats[s]["region"] == region]
        if not filtered_servers:
            raise ValueError("No servers in region {0}".format(region))

        selected_server = None
        min_load = float("inf")
        min_response_time = float("inf")

        for server in filtered_servers:
            server_data = self.server_stats[server]
            load = server_data["load"]
            response_time = server_data["response_time"]
            requests = server_data["requests"]
            last_used = server_data["last_used"]

            # Select server with lowest load and lowest response time.
            if load < min_load or (load == min_load and response_time < min_response_time):
                selected_server = server
                min_load = load
                min_response_time = response_time

            # If load and response time are tied, select server with fewer requests.
            elif load == min_load and response_time == min_response_time:
                if requests < self.server_stats[selected_server]["requests"]:
                    selected_server = server

        self.server_stats[selected_server]["load"] += 1
        self.server_stats[selected_server]["requests"] += 1
        self.server_stats[selected_server]["last_used"] = time.time()
        return selected_server

         def update_server_stats(self, server, response_time):
        self.server_stats[server]["load"] -= 1
        self.server_stats[server]["response_time"] = response_time

    def get_stats(self):
        server_stats = self.server_stats.values()
        stats = {
            "total_requests": sum(s["requests"] for s in server_stats),
            "servers": {},
            "regions": {},
        }
        for server, server_data in self.server_stats.items():
            region = server_data["region"]
            stats["servers"][server] = {
                "load": server_data["load"],
                "response_time": server_data["response_time"],
                "requests": server_data["requests"],
                "last_used": server_data["last_used"],
            }
            if region not in stats["regions"]:
                stats["regions"][region] = {
                    "servers": [],
                    "load": 0,
                    "requests": 0,
                    "response_time": 0,
                    "last_used": 0,
                }
            stats["regions"][region]["servers"].append(server)
            stats["regions"][region]["load"] += server_data["load"]
            stats["regions"][region]["requests"] += server_data["requests"]
            stats["regions"][region]["response_time"] += server_data["response_time"]
            stats["regions"][region]["last_used"] = max(stats["regions"][region]["last_used"], server_data["last_used"])

        return stats